# Advent of Code 2017 Puzzles

Yes, I'm a bit late on these.

## About

This repository contains solutions for the 2017 [Advent of Code](https://adventofcode.com/) puzzles.
Each contains nicely annotated Python code as well as the input file that I used to solve it.
Should you create your own account, you'll recieve a different input, though the code will still work.
Every AOC puzzle comes in two parts, though I've only included the source for part 1. I encourage you to build off of it to solve part 2.