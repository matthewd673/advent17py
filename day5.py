#https://adventofcode.com/2017/day/5

#load input file
f = open("input/day5.txt", 'r')
commands = f.readlines()

exitVal = len(commands) #the value that must be reached to exit

#loop through and turn array into integers
for i in range(len(commands)):
	commands[i] = int(commands[i])

pointer = 0 #to keep track of current position
lastPointer = 0 #to increment the last value

steps = 0 #count steps taken

while(pointer < exitVal): #loops forever until escaping
	#get current command value
	c = commands[pointer]

	#remember last position, then move
	lastPointer = pointer
	pointer += c

	#increment at last position
	commands[lastPointer] += 1

	#add another step
	steps += 1

print("Steps: " + str(steps))