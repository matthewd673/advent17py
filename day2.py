#https://adventofcode.com/2017/day/2

#load input file
f = open("input/day2.txt", 'r')
rows = f.readlines()

checksum = 0

#loop through rows
for i in range(len(rows)):
	cells = rows[i].split("\t") #split into cells by tab character
	
	#our min/max values (not set yet)
	minVal = 0
	maxVal = 0
	#loop through cells
	for j in range(len(cells)):
		#get the cell value as an integer
		cellVal = int(cells[j])
		#set the min and max to the first cell
		if j == 0:
			minVal = cellVal
			maxVal = cellVal
		else:
			#if current value is lower than min, set it to the min
			if minVal > cellVal:
				minVal = cellVal
			#if current value is higher than max, set it to the max
			if maxVal < cellVal:
				maxVal = cellVal
	
	#find the difference and add it to the checksum
	difference = maxVal - minVal
	checksum += difference

print("Checksum: " + str(checksum))