#https://adventofcode.com/2017/day/4

#load input file
f = open("input/day4.txt", 'r')
phrases = f.readlines()

safePhrases = []

#loop through phrases
for i in range(len(phrases)):
	phrase = phrases[i] #current phrase
	
	#create a list of words we've seen in the phrase
	seenWords = []

	safe = True

	#split into words and loop
	words = phrase.split(" ")
	for j in range(len(words)):
		#sometimes a newline character sneaks in, remove it
		word = words[j].replace("\n", "")
		#if this word has already been seen passphrase is unsafe and can be skipped
		if word in seenWords:
			safe = False
		else:
			seenWords.append(word) #unique, add to the list
	
	print(str(seenWords) + " : " + str(words))

	#if the phrase is still safe add to the list
	if safe == True:
		if phrase not in safePhrases:
			safePhrases.append(phrase)
		print("safe!!!\n")

print("Safe phrases: " + str(len(safePhrases)))