#https://adventofcode.com/2017/day/1

#load input file
f = open("input/day1.txt", 'r')
input = f.readlines()[0]

#split input into individual numbers
digits = list(input)

#prep total
total = 0

#loop through list of numbers
for i in range(len(digits)):
	#make sure theres another number to check
	if i < len(digits) - 1:
		#check if both numbers are the same
		if digits[i] == digits[i + 1]:
			#add to the total
			total += int(digits[i])
	else:
		#its the last digit, but if it matches the first it will still be added
		if digits[i] == digits[0]:
			total += int(digits[i])

#print the answer
print(total)